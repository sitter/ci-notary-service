<!--
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>
-->

# aabsigner.py - An AAB Signing Service

Provides code signing for Android app bundles (AAB).

## Prerequisites

The service needs an SFTP server for exchanging data with the clients.

Additionally, for signing AABs you need
* a key store file containing the certificate to sign the AABs with,
* a file containing the password to unlock the key store.

## Installation

### Install Bundletool

Bundletool is needed for verifying the application ID of AABs. Download the jar
file from the
[GitHub repository](https://github.com/google/bundletool/releases).

### Install the Service

Clone this repository on a server that shall provide the service,
create a virtual environment, and install the requirements.

```sh
git clone https://invent.kde.org/sysadmin/ci-notary-service.git aabsigner
cd aabsigner
python3 -m venv .venv
. .venv/bin/activate
pip3 install -r requirements.txt
```

### Configure the Service

Copy `aabsigner.sample.ini` to `aabsigner.ini` and replace the example values
with the appropriate values for your installation.

Then copy `aabsigner-projects.sample.yaml` to `aabsigner-projects.yaml` and
specify the settings for the projects you want to sign APKs for.

## Run the Service

Activate the virtual environment and start `aabsigner.py`.

```sh
cd ~/aabsigner
. .venv/bin/activate
python3 aabsigner.py --id workerA --config aabsigner.ini
```

where you replace `workerA` with a unique alpha-numeric ID for each instance
of `aabsigner.py` you start.

## Use the Service to Sign an APK

Clone this repository on the client, create a virtual environment, and install
the requirements.

```sh
git clone https://invent.kde.org/sysadmin/ci-notary-service.git aabsigner
cd aabsigner
python3 -m venv .venv
. .venv/bin/activate
pip3 install -r requirements.txt
```

Copy `signaab.sample.ini` to `signaab.ini` and replace the example values
with the appropriate values for your installation. In particular, the value
for `BasePath` must match the corresponding value in the settings of the
service.

Then run `signaab.py` to sign the APK

```sh
python3 signaab.py --config signaab.ini <aab>
```

## Managing the Service with systemd

We set up the service under the user's control with a per-user systemd instance.

Copy `aabsigner-user.sample.service` to `~/.config/systemd/user/aabsigner-user.service`
and modify it as needed. The example uses the username as ID of the service
worker. Make sure that the ID is unique if you run multiple service workers.

Start the service with
```sh
systemctl --user start aabsigner-user.service
```

If you want the service to be started automatically, then enable autostart with
```sh
systemctl --user enable aabsigner-user.service
```

Note: By default, the systemd user instance and thus the user-managed services are
started after the first login of a user and killed after the last session of the
user is closed. To start the user instance right after boot, and keep the systemd
user instance running after the last session closes you have to enable lingering
for the user:
```sh
loginctl enable-linger USERNAME
```

For more details on user-managed services see https://wiki.archlinux.org/title/Systemd/User.
