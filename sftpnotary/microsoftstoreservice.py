# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>

import json
import logging
from pathlib import Path
from tempfile import TemporaryDirectory

from sftpnotary import config, microsoftstoresubmission, projects
from sftpnotary.core import TaskProcessor, Worker
from sftpnotary.exceptions import Error
from sftpnotary.microsoftstoredata import PublishOnMicrosoftStoreRequest, PublishOnMicrosoftStoreResponse

log = logging.getLogger(__name__)


class PublishOnMicrosoftStoreProcessor(TaskProcessor):
    requestClass = PublishOnMicrosoftStoreRequest

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        credentialsPath = config.settings.getPath("MicrosoftStorePublishing", "Credentials")
        if credentialsPath is None:
            raise Error("Credentials not set in config.")
        with open(credentialsPath, "r") as credentialsFile:
            credentials = json.load(credentialsFile)
        self.tenantId = credentials["tenant_id"]
        self.clientId = credentials["client_id"]
        self.clientSecret = credentials["client_secret"]

    def getProjectSetting(self, name, required=True):
        value = projects.settings.get(self.request.projectPath, self.request.branch, name)
        if required and not value:
            raise Error(
                f"Project setting '{name}' not set for branch {self.request.branch} "
                f"of project {self.request.projectPath}"
            )
        return value

    def doProcess(self):
        appstreamId = self.getProjectSetting("appstreamid")
        keepProps = self.getProjectSetting("keep", required=False) or []
        if not isinstance(keepProps, list):
            raise Error("Project setting 'keep' must be a list")
        keepProps = [s.lower() for s in keepProps]

        with TemporaryDirectory() as workPathName:
            workPath = Path(workPathName)

            localAppxFilePath = workPath / self.request.fileName
            self.sftp.download(self.task.path() / self.request.fileName, localAppxFilePath)

            microsoftstoresubmission.submitApp(
                localAppxFilePath,
                appstreamId=appstreamId,
                keep=keepProps,
                tenantId=self.tenantId,
                clientId=self.clientId,
                clientSecret=self.clientSecret,
            )

        return PublishOnMicrosoftStoreResponse()


class PublishOnMicrosoftStoreWorker(Worker):
    processorClass = PublishOnMicrosoftStoreProcessor
